package setup;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeSuite;

import io.cucumber.java.*;
import io.github.bonigarcia.wdm.WebDriverManager;
public class initialSetup {
	
	
	private static WebDriver driver;
	
	@BeforeAll
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        setDriver(driver);  
    }
    
    public static void setDriver(WebDriver driverValue) {
    	driver = driverValue;
    }
    
    public static WebDriver getDriver() {
    	
    	return driver;
    }

}
