package Runner;

import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import io.cucumber.junit.*;


@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/java/features/",
		glue= {"stepDefinition"},
		plugin = { "pretty" },
		monochrome = true
		)
 
public class TestRunner {
	
}